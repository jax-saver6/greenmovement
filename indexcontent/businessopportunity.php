<!DOCTYPE html>
<html> 
	<head>
		<title></title> 
		<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,700' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Oswald:400,700,300' rel='stylesheet' type='text/css'>
		<link rel='stylesheet' id='rs-plugin-settings-css'  href='http://hahnenviro.caveman-tech.com/wp-content/plugins/revslider/rs-plugin/css/settings.css?rev=4.6.0&#038;ver=4.0.11' type='text/css' media='all' />		
		<style>
			body, div, h1, h2, h3, h4, h5, h6 {margin: 0; padding: 0;} 
			.s6Container {width: 100%;}
			.s6Box {width: 960px; margin: 0 auto;}
			.s6Box1 {width: 100%;}
			.s6Box3 {width: 100%;}
			.s6Box3a {width: 33%; display: inline-block;}
			.s6Box3b {width: 33%; display: inline-block;}
			.s6Box3c {width: 33%; display: inline-block;}

			.myIcoContainer {height: 220px;} 
			.myIcoLine {border: 6px solid #A6DF43; width: 102px; height: 102px; margin: 0 auto; background-color: #FFF; -moz-border-radius: 100%; -webkit-border-radius: 100%; border-radius: 100%; -khtml-border-radius: 100%;}
			.myIco {border: 6px solid #FFF; width: 90px; height: 90px; margin: 0 auto; background-color: #A6DF43; -moz-border-radius: 100%; -webkit-border-radius: 100%; border-radius: 100%; -khtml-border-radius: 100%;}
			.myIco img {margin: 20px 0;}
			.myIcoTitle {margin: 10px 0; font-size: 20px;}
			.myIcoDescription {font-size: 14px;}

			#myImgOilCollection {float: left; margin: 0 10px 10px 0;}
			#myImgRecycle {float: right; margin: 0 0 10px 10px;}
			#myImgStartNow {margin: 50px 0 30px 0; width: 200px;}
			.myImg100 {width: 100%;}
			.myImg75 {width: 75%; max-width: 590px;} 
			.myWid100Mar0Aut {width: 100%; margin: 0 auto; text-align: center;}

			.s6BoxContent {width: 90%; margin: 0 auto;}

			.mybox1 {font-family: 'Open Sans', sans-serif; font-weight: 400; text-align: center; color: #FFFFFF; background:#333333 url(http://ecorecycle.premiumcoding.com/wp-content/uploads/2013/12/background-recycle-icon1.png) 50% 0;background-size:cover; padding:57px 0 57px 0;}
			.mybox2 {font-family: 'Open Sans', sans-serif; font-weight: 400; text-align: center; color: #333333; background:#FFFFFF; padding:27px 0 67px 0;}
			.mybox3 {font-family: 'Open Sans', sans-serif; font-weight: 400; text-align: center; color: #FFFFFF; background:#333333 url(http://ecorecycle.premiumcoding.com/wp-content/uploads/2013/12/background-recycle-icon1.png) 50% 0;background-size:cover; padding:57px 0 57px 0;}
			.mybox4 {font-family: 'Open Sans', sans-serif; font-weight: 400; text-align: left; color: #333333; background:#FFFFFF; padding:27px 0 67px 0;}
			.mybox5 {font-family: 'Open Sans', sans-serif; font-weight: 400; text-align: center; color: #FFFFFF; background:#fff url(http://ecorecycle.premiumcoding.com/wp-content/uploads/2013/12/quote-background-eco.jpg) 50% 0;background-size:cover;border-top:0px solid #fff;border-bottom:0px solid #fff;padding:100px 0 120px 0; padding:87px 0 87px 0;}
			
			.myVid {float: left; text-align: left; width: 50%; margin-bottom: 15px;}
 
			.arrowDown {margin: 0 auto; width: 0; height: 0; border-left: 50px solid transparent; border-right: 50px solid transparent; border-top: 50px solid #333333;}

			.contentBig {font-size: 42px !important; line-height: 110%; color: #333333;}
			.fontBig {font-size: 72px !important; line-height: 110%; color: #FFFFFF;}
			.fontSmall {font-size: 14px;}
			.fontBold {font-weight: bold;}
			.saver6Button { margin-top: 10px; background-color:transparent; -moz-border-radius:28px; -webkit-border-radius:28px; border-radius:28px; border:1px solid #FFFFFF; display:inline-block; cursor:pointer; color:#ffffff; font-family:'Open Sans'; font-size:17px; padding:16px 31px; text-decoration:none; }
			.saver6Button:hover { background-color:transparent; }
			.saver6Button:active { position:relative; top:1px; }


			.sliderBig {font-family: 'Oswald', sans-serif; font-weight: 700; font-size: 60px; text-transform: uppercase;}
			.sliderSmall {font-family: 'Oswald', sans-serif; font-weight: 300; font-size: 20px;}

			.fontGreen {color: #93BE55;}
			
			.myboxa0 {font-family: 'Open Sans', sans-serif; font-weight: 400; text-align: left; color: #333333; background:#FFFFFF; padding:77px 0 77px 0;} /* new */
			.myboxa1 {font-family: 'Open Sans', sans-serif; font-weight: 400; text-align: left; color: #333333; background:#009639; padding:87px 0 87px 0;} /* new */
			.myboxa2 {font-family: 'Open Sans', sans-serif; font-weight: 400; text-align: left; color: #333333; background:#FFFFFF; padding:77px 0 77px 0;} /* new */

			#myImgA1 {width: 350px; float: right; bottom: 0px; margin: 0 0 10px 10px;} /* new 1 */
			#myImgA2 {width: 120px; float: right; margin: 0 0 10px 10px;} /* new 2 */


			@media screen and (min-width:320px) and (max-width: 479px)  { 
				.s6Box {width: 100%;}	
				.s6Box3a {display: block; width: 100%;}
				.s6Box3b {display: block; width: 100%;}
				.s6Box3c {display: block; width: 100%;}

				.myVid {float: none; width: 100%; margin-bottom: 15px;}

				.myIcoTitle {font-size: 14px;}
				.myIcoDescription {font-size: 10px;}

				.fontBig {font-size: 18px !important;}
				.fontSmall {font-size: 10px;}  
				.contentBig {font-size: 18px !important;}

				.sliderBig {font-size: 18px;}
				.sliderSmall {font-size: 10px;}

				#myImgOilCollection {width: 120px;}
				#myImgRecycle {width: 120px;}
				#myImgStartNow {width: 120px;}

				#myImgA1 {width: 120px; float: right; margin: 0 0 10px 10px;} /* new 1 */
				#myImgA2 {width: 80px; float: right; margin: 0 0 10px 10px;} /* new 2 */
			}

			@media screen and (min-width:480px) and (max-width: 768px)  { 
				.s6Box {width: 100%;}	
				.s6Box3a {display: block; width: 100%;}
				.s6Box3b {display: block; width: 100%;}
				.s6Box3c {display: block; width: 100%;}

				.myVid {float: none; width: 100%; margin-bottom: 15px;}

				.myIcoTitle {font-size: 18px;}
				.myIcoDescription {font-size: 12px;}

				.fontBig {font-size: 36px !important;}
				.fontSmall {font-size: 12px;} 
				.contentBig {font-size: 36px !important;}

				.sliderBig {font-size: 36px;}
				.sliderSmall {font-size: 12px;}

				#myImgOilCollection {width: 170px;}
				#myImgRecycle {width: 170px;}
				#myImgStartNow {width: 170px;}

				#myImgA1 {width: 170px; float: right; margin: 0 0 10px 10px;} /* new 1 */
				#myImgA2 {width: 100px; float: right; margin: 0 0 10px 10px;} /* new 2 */
			}
		</style>
 
	</head>
	<body>



		<div class="s6Container myboxa0">
			<div class="s6Box">  
				<div class="s6Box1 s6BoxContent"> 
					<div class="contentBig"> USED COOKING OIL BUSINESS OPPORTUNITIES </div>
					<br />
					Hahn Group have Business Opportunities available in our ‘Used Cooking Oil’ collection network. We are providing prospective business owners with a profitable, environmentally friendly customised opportunity, with the ability to grow their business to no limits. Market research shows that the worlds population are dining out more than ever before. Restaurants are increasing their cooking oil usage by 15% every year to keep up with the demands of our population.
					<br /> <br />
					Some of our options provide customised solutions for those who may already own their own truck and wish to further utilise it, as well as finance options.
					<img id="myImgA1" src="img/a1Trailer-package.png" alt=" Saver6.com, Hahn Group, Oil Collections " /> 
					<br /> <br />
					Options:
					<ul class="">
						<li>Truck including Vacuum filtration system with/or without existing client network</li>
						<li>No Truck, Vacuum filtration system with/or without existing client network</li>
						<li>Finance packages are available.</li>
					</ul> <br /> 
					Please register your interest online or call us to discuss options available.
				</div> 
			</div>
		</div>



		<div class="s6Container myboxa1" style="font-family: 'Open Sans', sans-serif; font-weight: 400; text-align: left; color: #333333; background: #009639; padding:87px 0 87px 0; margin: 50px 0;">
			<div class="s6Box">  
				<div class="s6Box1 s6BoxContent"> 
					<iframe width="100%" height="350" style="margin-left: 0px;" src="https://www.youtube.com/embed/wcFGEIxa6-0" frameborder="0" allowfullscreen></iframe>
				</div> 
			</div>
		</div>
					


		<div class="s6Container myboxa2">
			<div class="s6Box">  
				<div class="s6Box1 s6BoxContent"> 					
					<div class="contentBig"> USED COOKING OIL COLLECTION </div>
					<br /><br />
					<img id="myImgA2" src="img/a2oil-storage-tank.png" alt=" Saver6.com, Hahn Group, Oil Collections " /> 
					Hahn Group offers a service in the collection of ‘Used Cooking Oil’, which in turn is re-used to produce alternative fuels, such as burner fuel and biodiesel. With the introduction of stringent Environmental Laws around waste oil pollution, Hahn Group has given the hospitality industry the ability to easily recycle their used cooking oil, knowing that it is being reused.
					<br /><br />
					Hahn Group has a large existing network of modern customised ‘Used Cooking Oil’ trucks approved by the ‘Department of Environment’, providing regular collection services to Restaurants, Shopping Centres, Cafes and Hospitals, to name a few, throughout the hospitality industry. We provide our hospitality customers with a vacuum filtration unit on wheels in order to make changing their cooking oil, a clean hassle-free task, eliminating any heavy lifting of drums. We assess your current weekly volumes and provide you with a vacuum filtration unit sized to suit. 
				</div> 
			</div>
		</div>



	</body> 
</html>