<!DOCTYPE html>
<html>
	<head>
		<title></title> 
		<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,700' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Oswald:400,700,300' rel='stylesheet' type='text/css'>
		<link rel='stylesheet' id='rs-plugin-settings-css'  href='http://hahnenviro.caveman-tech.com/wp-content/plugins/revslider/rs-plugin/css/settings.css?rev=4.6.0&#038;ver=4.0.11' type='text/css' media='all' />		

        <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon"> 
        <link rel="icon" href="img/favicon.ico" type="image/x-icon">  

        <style>

			body, div, h1, h2, h3, h4, h5, h6 {margin: 0; padding: 0;} 
			.s6Container {width: 100%;}
			.s6Box {width: 960px; margin: 0 auto;}
			.s6Box1 {width: 100%;}
			.s6Box3 {width: 100%;}
			.s6Box3a {width: 33%; display: inline-block;}
			.s6Box3b {width: 33%; display: inline-block;}
			.s6Box3c {width: 33%; display: inline-block;}

			.myIcoContainer {height: 220px;} 
			.myIcoLine {border: 6px solid #A6DF43; width: 102px; height: 102px; margin: 0 auto; background-color: #FFF; -moz-border-radius: 100%; -webkit-border-radius: 100%; border-radius: 100%; -khtml-border-radius: 100%;}
			.myIco {border: 6px solid #FFF; width: 90px; height: 90px; margin: 0 auto; background-color: #A6DF43; -moz-border-radius: 100%; -webkit-border-radius: 100%; border-radius: 100%; -khtml-border-radius: 100%;}
			.myIco img {margin: 20px 0;}
			.myIcoTitle {margin: 10px 0; font-size: 20px;}
			.myIcoDescription {font-size: 14px;}

			#myImgOilCollection {float: left; margin: 0 10px 10px 0;}
			#myImgRecycle {float: right; margin: 0 0 10px 10px;}
			#myImgStartNow {margin: 50px 0 30px 0; width: 200px;}
			.myImg100 {width: 100%;}
			.myImg75 {width: 75%; max-width: 590px;} 
			.myWid100Mar0Aut {width: 100%; margin: 0 auto; text-align: center;}

			.s6BoxContent {width: 90%; margin: 0 auto;}

			.mybox1 {font-family: 'Open Sans', sans-serif; font-weight: 400; text-align: center; color: #FFFFFF; background:#333333 url(http://ecorecycle.premiumcoding.com/wp-content/uploads/2013/12/background-recycle-icon1.png) 50% 0;background-size:cover; padding:57px 0 57px 0;}
			.mybox2 {font-family: 'Open Sans', sans-serif; font-weight: 400; text-align: center; color: #333333; background:#FFFFFF; padding:27px 0 67px 0;}
			.mybox3 {font-family: 'Open Sans', sans-serif; font-weight: 400; text-align: center; color: #FFFFFF; background:#333333 url(http://ecorecycle.premiumcoding.com/wp-content/uploads/2013/12/background-recycle-icon1.png) 50% 0;background-size:cover; padding:57px 0 57px 0;}
			.mybox4 {font-family: 'Open Sans', sans-serif; font-weight: 400; text-align: left; color: #333333; background:#FFFFFF; padding:27px 0 67px 0;}
			.mybox5 {font-family: 'Open Sans', sans-serif; font-weight: 400; text-align: center; color: #FFFFFF; background:#fff url(http://ecorecycle.premiumcoding.com/wp-content/uploads/2013/12/quote-background-eco.jpg) 50% 0;background-size:cover;border-top:0px solid #fff;border-bottom:0px solid #fff;padding:100px 0 120px 0; padding:87px 0 87px 0;}

			.myboxc1 {font-family: 'Open Sans', sans-serif; font-weight: 400; text-align: left; color: #333333; background:#009639; padding:87px 0 87px 0;} /* new */
			
			.myboxTop {}
			.myboxNav {font-family: 'Open Sans', sans-serif; font-weight: 300; text-align: center; color: #FFFFFF; background-color: #333333;}
			
			.myVid {float: left; text-align: left; width: 50%; margin-bottom: 15px;}
 
			.arrowDown {margin: 0 auto; width: 0; height: 0; border-left: 50px solid transparent; border-right: 50px solid transparent; border-top: 50px solid #333333;}

			.contentBig {font-size: 32px !important; line-height: 110%; color: #333333;}
			.fontBig {font-size: 32px !important; line-height: 110%; color: #FFFFFF;}
			.fontSmall {font-size: 14px;}
			.fontBold {font-weight: bold;}
			.saver6Button { margin-top: 10px; background-color:transparent; -moz-border-radius:28px; -webkit-border-radius:28px; border-radius:28px; border:1px solid #FFFFFF; display:inline-block; cursor:pointer; color:#ffffff; font-family:'Open Sans'; font-size:17px; padding:16px 31px; text-decoration:none; }
			.saver6Button:hover { background-color:transparent; }
			.saver6Button:active { position:relative; top:1px; }


			.sliderBig {font-family: 'Oswald', sans-serif; font-weight: 700; font-size: 60px; text-transform: uppercase;}
			.sliderSmall {font-family: 'Oswald', sans-serif; font-weight: 300; font-size: 20px;}

			.fontGreen {color: #93BE55;}

			.overlay1{background-color: rgba(0,0,0,.75); position:absolute; width:100%; height: 100%;}

			.swiper-container{width:100%;height:380px}.swiper-slide{text-align:center;font-size:18px;background:#fff;display:-webkit-box;display:-ms-flexbox;display:-webkit-flex;display:flex;-webkit-box-pack:center;-ms-flex-pack:center;-webkit-justify-content:center;justify-content:center;-webkit-box-align:center;-ms-flex-align:center;-webkit-align-items:center;align-items:center}#lab_video_text_overlay h2{font-size:4em}#lab_video_text_overlay .btn{border-radius:0;text-transform:uppercase;font-weight:200}#lab_video_text_overlay .btn-warning{background-color:#F5D328}#lab_video_text_overlay{background-color: transparent; padding-top: 80px; position:absolute;color: #fff;width:100%; height: 100%; bottom-top:25px}#lab_video_text_overlay .container{text-align:center;margin:0 auto}@media screen and (max-width:991px){#lab_video_text_wrap{height:200px}}@media screen and (max-width:650px){#lab_video_text_wrap{height:150px}#lab_video_text_overlay h2{font-size:2em}#lab_video_text_overlay p{font-size:.9em}}

			.myImgC1 {width: 75%; min-width: 180px} /* new */
			.s6BoxCContent {text-align: center;} /* new */

			@media screen and (min-width:320px) and (max-width: 479px)  { 
				.s6Box {width: 100%;}	
				.s6Box3a {display: block; width: 100%;}
				.s6Box3b {display: block; width: 100%;}
				.s6Box3c {display: block; width: 100%;}

				.myVid {float: none; width: 100%; margin-bottom: 15px;}

				.myIcoTitle {font-size: 14px;}
				.myIcoDescription {font-size: 12px;}

				.fontBig {font-size: 18px !important;}
				.fontSmall {font-size: 10px;}  
				.contentBig {font-size: 18px !important;}

				.sliderBig {font-size: 18px;}
				.sliderSmall {font-size: 12px;}

				#myImgOilCollection {width: 120px;}
				#myImgRecycle {width: 120px;}
				#myImgStartNow {width: 120px;}

				.swiper-container{width:100%;height:200px}
			}

			@media screen and (min-width:480px) and (max-width: 768px)  { 
				.s6Box {width: 100%;}	
				.s6Box3a {display: block; width: 100%;}
				.s6Box3b {display: block; width: 100%;}
				.s6Box3c {display: block; width: 100%;}

				.myVid {float: none; width: 100%; margin-bottom: 15px;}

				.myIcoTitle {font-size: 18px;}
				.myIcoDescription {font-size: 12px;}

				.fontBig {font-size: 36px !important;}
				.fontSmall {font-size: 12px;} 
				.contentBig {font-size: 36px !important;}

				.sliderBig {font-size: 36px;}
				.sliderSmall {font-size: 12px;}

				#myImgOilCollection {width: 170px;}
				#myImgRecycle {width: 170px;}
				#myImgStartNow {width: 170px;}

				.swiper-container{width:100%;height:220px}
			}        
        </style>
	</head>
	<body>

 
	<div>Test</div>



		<div class="s6Container mybox0"> 

 
			<!-- reference examples at http://www.idangero.us/swiper/demos/#.Vl8kD9-rRHc -->
			<section id="lab_video_slider">
			  <div class="container-fluid">
			    <div class="row">
			      <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">

			      <!-- Link Swiper's CSS -->
			      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.2.5/css/swiper.min.css">

			      <!-- Swiper -->
			      <div class="swiper-container">
			        <div class="swiper-wrapper">
			          <div id="slide_one" class="swiper-slide">
						  <div class="overlay1"> 
						  </div>
						  <div id="lab_video_text_overlay">
						    <div class="container">
						      <div class="sliderBig"> Business Opportunity </div>
						      <div class="sliderSmall"> Take care of our environment while earning. </div>  
						      <div class="saver6Button"> Read More </div> 
						    </div>
						  </div>
			  <!-- end #lab_overlay -->
			            <!-- VIDEO -->

			            <video class="slider-video" width="100%" preload="auto" muted loop="" autoplay="" style="visibility: visible; width: 100%;" poster="//dl.dropbox.com/s/pjopy0mu4klisat/working-with-espresso.jpg">
			              <!-- <source src="//dl.dropbox.com/s/931244iox7i0fpk/working-with-espresso.mp4" type="video/mp4"> -->
			                <source src="img/home-vid-bg-01.webm" type="video/webm">
			                  <!-- <source src="//dl.dropbox.com/s/p37f0avio0x6bs8/working-with-espresso.ogv" type="video/ogg">  -->
			            </video>
			            <!-- END VIDEO -->
			          </div>

			          <div id="slide_two" class="swiper-slide">
						  <div class="overlay1"> 
						  </div>
						  <div id="lab_video_text_overlay">
						    <div class="container">
						      <div class="sliderBig"> Used Cooking Oil Collection </div>
						      <div class="sliderSmall"> Turns from waste oil to a re-usable engine-friendly bio diesel for vehicles. </div>   
						      <div class="saver6Button"> Read More </div> 
						    </div>
						  </div>
			  <!-- end #lab_overlay -->

						  <div class="overlay1"> 
						  </div>
			            <video class="slider-video" width="100%" preload="auto" loop="" autoplay="" style="visibility: visible; width: 100%;" poster="//dl.dropbox.com/s/ijyaav8qzkdtyt5/lab-coding-screenshot.jpg">
			              
			      <!-- <source type="video/mp4" src="//dl.dropbox.com/s/5z4jupv385iboiu/lab-coding.mp4"> -->
			        <!-- <source src="//dl.dropbox.com/s/5z4jupv385iboiu/lab-coding.mp4" type="video/mp4"> -->
			          <source src="img/home-vid-bg-02.webm" type="video/webm">
			            <!-- <source src="//dl.dropbox.com/s/7bq2tyma5de88bt/lab-coding.ogg" type="video/ogg"> -->
			            </video>
			          
			          </div>

			          <div id="slide_three" class="swiper-slide">
						  <div class="overlay1"> 
						  </div>
						  <div id="lab_video_text_overlay">
						    <div class="container">
						      <div class="sliderBig">Lorum Ipsum Deloras </div>
						      <div class="sliderSmall">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc molestie</div>   
						      <div class="saver6Button"> Contact Us </div>
						    </div>
						  </div>
			  <!-- end #lab_overlay -->
			             <!-- VIDEO -->
			            <video class="slider-video" width="100%" preload="auto" loop="" autoplay="" style="visibility: visible; width: 100%;" poster="//dl.dropbox.com/s/5aeyg9ntj1fs8v1/typing-numbers.jpg">
			              <!-- <source src="//dl.dropbox.com/s/xmosdvoleruexky/typing-numbers.mp4" type="video/mp4"> -->
			                <source src="img/home-vid-bg-03.webm" type="video/webm">
			                  <!-- <source src="//dl.dropbox.com/s/5pyuc35wnv5khe1/typing-numbers.ogv" type="video/ogg"> -->
			            </video>
			            <!-- END VIDEO -->
			          </div> 




			          <div id="slide_four" class="swiper-slide">
						  <div class="overlay1"> 
						  </div>
						  <div id="lab_video_text_overlay">
						    <div class="container">
						      <div class="sliderBig"> Commercial Kitchen Cleaning Services </div>
						      <div class="sliderSmall"> We offer a cost effective solution to maintaining your kitchen investments. </div>   
						      
						    </div>
						  </div>
			  <!-- end #lab_overlay -->
			             <!-- VIDEO -->
			            <video class="slider-video" width="100%" preload="auto" loop="" autoplay="" style="visibility: visible; width: 100%;" poster="//dl.dropbox.com/s/5aeyg9ntj1fs8v1/typing-numbers.jpg">
			              <!-- <source src="//dl.dropbox.com/s/xmosdvoleruexky/typing-numbers.mp4" type="video/mp4"> -->
			                <source src="http://hahngroup.com.au/businessopportunity/wp-includes/images/hahn/home-vid-bg-04.webm" type="video/webm">
			                  <!-- <source src="//dl.dropbox.com/s/5pyuc35wnv5khe1/typing-numbers.ogv" type="video/ogg"> -->
			            </video>
			            <!-- END VIDEO -->
			          </div> 


			        </div>
			        <!-- Add Pagination -->
			        <div class="swiper-pagination" style="display: none;"></div>
			        <!-- Add Arrows -->
			        <div class="swiper-button-next" style="color: #FFF; fill: #FFF;"></div>
			        <div class="swiper-button-prev" style="color: #FFF; fill: #FFF;"></div>
			      </div>

			      <!-- Swiper JS -->
			      <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.2.5/js/swiper.min.js"></script>

			      <!-- Initialize Swiper -->
			      <script>
			        var swiper = new Swiper('.swiper-container', {
			            pagination: '.swiper-pagination',
			            paginationClickable: true,
			            nextButton: '.swiper-button-next',
			            prevButton: '.swiper-button-prev',
			            spaceBetween: 0,
			            autoplay: 5000,
			            autoplayDisableOnInteraction: true
			        });
			      </script>
			    </div>
			    <!-- end .row -->
			  </div>
			  <!-- end .container-->
			  </div>
			  <!-- end #section -->

 
		</div>

		

		<div class="s6Container mybox1">
			<div class="s6Box">
				<div class="s6Box1">
					<div class="fontBig">COOKING OIL <span class="fontGreen">RECYCLING</span></div>
					<div class="fontSmall">COMMUNITY AND BUSINESS WASTE-TO-REUSE INITIATIVE</div>
				</div>
			</div>
		</div>
		<div class="arrowDown"></div>

		<div class="s6Container mybox2">
			<div class="s6Box"> 
				<div class="s6Box3">
					<div class="s6Box3a myIcoContainer">
						<div class="myIcoLine"> <div class="myIco"> <img src="img/ico1.png" /> </div> </div>
						<div class="myIcoTitle"> Energy Services </div>
						<div class="myIcoDescription"> We are the largest Biodiesel producer in Queensland capturing energy and improving client’s profitability. </div>
					</div>
					<div class="s6Box3b myIcoContainer">
						<div class="myIcoLine"> <div class="myIco"> <img src="img/ico2.png" /> </div> </div>
						<div class="myIcoTitle"> Prompt, reliable pick up </div>
						<div class="myIcoDescription"> Weekly or fortnightly pick up service depending on used cooking oil volume. <br /> &nbsp; </div>
					</div>
					<div class="s6Box3c myIcoContainer">
						<div class="myIcoLine"> <div class="myIco"> <img src="img/ico3.png" /> </div> </div>
						<div class="myIcoTitle"> Easy to Use Suck Equipment </div>
						<div class="myIcoDescription"> We has manufactured an easy to use vacuum suck unit. You can install in less than 1 minute. <br /> &nbsp; </div>
					</div>
				</div>
			</div>
		</div>

		<div class="s6Container mybox3">
			<div class="s6Box">
				<div class="s6Box1">
					<div class="fontBig">NEW RECYCLE <span class="fontGreen">IDEA</span></div>
					<div class="fontSmall">OUR USED COOKING OIL BUSINESS DIVISION PROVIDES A USED COOKING OIL COLLECTION SERVICE YOU CAN RELY ON!</div>
				</div>
			</div>
		</div>
		<div class="arrowDown"></div>

		<div class="s6Container mybox4">
			<div class="s6Box">  
				<div class="s6Box1 s6BoxContent"> 
					Hahn Group is the largest privately owned, Australian company providing environmental solutions to a number of industries, namely the Mining industry and the Hospitality industry. Hahn Group specialises in recycle and reuse of various forms of waste, providing sustainable customised environmental solutions reducing our country's reliance on fossil fuels.
					<br /> <br />
					<img id="myImgOilCollection" src="http://hahnenviro.caveman-tech.com/wp-content/uploads/2013/12/cooking-oil-collection-vacuum.png" alt=" Saver6.com, Hahn Group, Oil Collections " /> 
					Prompt, reliable pick up – weekly or fortnightly pick up service depending on used cooking oil volume.
					Hahn Energy Services collects, reprocesses and recycles used cooking oil, vegetable oil and fats as environmentally friendly biodiesel. We are specialist in waste resource recovery and recycling ensuring renewable cooking oils can be reused as a valuable energy source. We collect used cooking oils from restaurants, fast food chains, food takeaway shops, fish and chip shops, shopping centers and food manufactures.
					<br /> <br />
					<br /> <br />
					<img id="myImgRecycle" src="http://hahnenviro.caveman-tech.com/wp-content/uploads/2012/04/portfolio-eco-image-31-280x220.jpg" alt=" Saver6.com, Hahn Group, Oil Collections " /> 
					Hahn Energy is the largest biodiesel producer in Queensland producing large volumes of alternative fuel from recycled cooking oil. We have established networks of reuse programs throughout Australia ensuring our biodiesel is all reused reducing our nation’s reliance on fossil fuels. Our dedication to cooking oil recovery prevents this waste product potentially polluting our environment and wildlife!
					<br /> <br />
					Easy to Use Suck Equipment – Hahn Energy Services has manufactured an easy to use vacuum suck unit. Simply wheel the unit up to your fryer, place the hose tip into the fryer and vacuum out the oil in less than 1 minute. This unit will be pumped out by our waste collection truck on its scheduled run or swap over with a clean unit. Warm or cold oil can be sucked up with this new unit. Lifting of drums is eliminated, OH&S is enhanced and stock management is improved.
					<br /> <br />
					We have purpose built collection trucks that meet EPA’s requirements for waste collection and our re-refining plants hold the necessary State and Federal licenses for alternative fuel production.
				</div> 
			</div>
		</div>



		<div class="s6Container myboxc1">
			<div class="s6Box">  
				<div class="s6Box1 s6BoxCContent"> 
					<img class="myImgC1" src="img/equips.png" alt=" Saver6.com, Hahn Group, Oil Collections " /> 
				</div> 
			</div>
		</div>

		<div class="s6Container mybox5">
			<div class="s6Box">
				<div class="s6Box1">
					<div class="fontBig fontBold"> START RECYCLING NOW </div>
					<div class="fontSmall fontBold"> Join our Climate Friendly Waste-to-reuse Initiative Online and have your used cooking oil recycled. </div>
					<img id="myImgStartNow" src="http://hahnenviro.caveman-tech.com/wp-content/uploads/hahn-logo.png" alt=" Saver6.com, Hahn Group, Start Recycling Now " /> <br />
					<button class="saver6Button"> Contact Us </button> 
				</div>
			</div>
		</div>

	</body> 
</html>